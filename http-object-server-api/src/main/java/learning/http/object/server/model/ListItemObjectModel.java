package learning.http.object.server.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

public class ListItemObjectModel implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;
    private String name;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof ListItemObjectModel)) return false;

        ListItemObjectModel that = (ListItemObjectModel) o;

        return new EqualsBuilder().append(getId(), that.getId()).append(getName(), that.getName()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(getId()).append(getName()).toHashCode();
    }

    @Override
    public String toString() {
        return "ListItemObjectModel{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
