package learning.http.object.server.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Arrays;

public class NewObjectModel implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;
    private byte[] bytes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof NewObjectModel)) return false;

        NewObjectModel that = (NewObjectModel) o;

        return new EqualsBuilder().append(getName(), that.getName()).append(getBytes(), that.getBytes()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(getName()).append(getBytes()).toHashCode();
    }

    @Override
    public String toString() {
        return "NewObjectModel{" +
                "name='" + name + '\'' +
                ", bytes=" + Arrays.toString(bytes) +
                '}';
    }
}
