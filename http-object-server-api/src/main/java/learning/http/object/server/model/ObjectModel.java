package learning.http.object.server.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.Arrays;

public class ObjectModel implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String name;
    private byte[] bytes;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof ObjectModel)) return false;

        ObjectModel that = (ObjectModel) o;

        return new EqualsBuilder().append(getId(), that.getId()).append(getName(), that.getName()).append(getBytes(), that.getBytes()).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(getId()).append(getName()).append(getBytes()).toHashCode();
    }

    @Override
    public String toString() {
        return "ObjectModel{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", bytes=" + Arrays.toString(bytes) +
                '}';
    }
}
