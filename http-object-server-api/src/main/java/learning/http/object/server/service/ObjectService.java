package learning.http.object.server.service;

import learning.http.object.server.model.ListItemObjectModel;
import learning.http.object.server.model.NewObjectModel;
import learning.http.object.server.model.ObjectModel;

import java.util.List;

public interface ObjectService {

    String create(NewObjectModel newObjectModel);

    void update(String id, NewObjectModel newObjectModel);

    void delete(String id);

    List<ListItemObjectModel> all();

    ObjectModel get(String id);

}
