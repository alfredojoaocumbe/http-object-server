package learning.http.object.server.service;

import learning.http.object.server.model.ListItemObjectModel;
import learning.http.object.server.model.NewObjectModel;
import learning.http.object.server.model.ObjectModel;

import java.util.List;

public class ObjectServiceImpl implements ObjectService{
    @Override
    public String create(NewObjectModel newObjectModel) {
        return null;
    }

    @Override
    public void update(String id, NewObjectModel newObjectModel) {

    }

    @Override
    public void delete(String id) {

    }

    @Override
    public List<ListItemObjectModel> all() {
        return null;
    }

    @Override
    public ObjectModel get(String id) {
        return null;
    }
}
