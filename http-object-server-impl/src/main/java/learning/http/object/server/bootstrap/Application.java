package learning.http.object.server.bootstrap;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Application {

    public static void main(String[] args) {
        try (ServerSocket server = new ServerSocket(2021)) {
            Socket connection = server.accept();
            InputStream inputStream = connection.getInputStream();
            InputStreamReader reader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(reader);
            bufferedReader.lines().forEach(System.out::println);
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
}
